#!/bin/sh

echo "
development:
  root: $ROOT_URL
  openid_root: $OPENID_ROOT
  this_server: $THIS_SERVER
  cookie_domain: $COOKIE_DOMAIN
  stats:
    server: $STATS_SERVER
    token: $STATS_TOKEN
  session_secret: $SESSION_SECRET
  jwt_key: $JWT_KEY
  ga_id: $GA_ID
  credit: <a href="http://indiewebcamp.com/sponsors">sponsors</a> of IndieWebCamp events
  redis:
    host: $REDIS_HOST
    port: $REDIS_PORT
  brevo:
    api_key: $BREVO_API_KEY
  mail:
    from: $MAIL_FROM
  providers:
    github:
      client_id: $GITHUB_CLIENT_ID
      client_secret: $GITHUB_CLIENT_SECRET
    gitlab:
      client_id: $GITLAB_CLIENT_ID
      client_secret: $GITLAB_CLIENT_SECRET
" > config.yml

set -e

if [ -f tmp/pids/server.pid ]; then
  rm tmp/pids/server.pid
fi

RACK_ENV=development

bundle exec thin -p 9007 --threaded start
