FROM ruby:3.2-alpine

ENV BUNDLER_VERSION=2.4.1

RUN apk add --update --no-cache \
      binutils-gold \
      build-base \
      curl \
      file \
      g++ \
      gcc \
      git \
      less \
      libstdc++ \
      libffi-dev \
      libc-dev \ 
      linux-headers \
      libxml2-dev \
      libxslt-dev \
      libgcrypt-dev \
      make \
      netcat-openbsd \
      openssl \
      pkgconfig \
      postgresql-dev \
      tzdata

RUN gem install bundler -v 2.4.1

WORKDIR /app

COPY Gemfile Gemfile.lock ./

RUN bundle config build.nokogiri --use-system-libraries

RUN bundle install
RUN bundle check

COPY . ./ 

RUN update-ca-certificates -f
RUN rm ./lib/ca-bundle.crt
RUN cp /etc/ssl/certs/ca-certificates.crt ./lib/ca-bundle.crt

ENTRYPOINT ["/bin/sh", "-e", "./entrypoints/docker-entrypoint.sh"]
