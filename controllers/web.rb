class Controller < Sinatra::Base

  get '/?' do
    title "Skribilo - Sign in with your domain name"
    erb :index
  end

  get '/setup/?' do
    title "Skribilo Documentation - Sign in with your domain name"
    erb :setup_instructions
  end

  get '/faq/?' do
    title "IndieAuth FAQ"
    erb :faq
  end

  get '/developers/?' do
    title "Skribilo for Developers"
    erb :developers
  end

  get '/sign-in/?' do
    title "Skribilo Sign-In"
    erb :sign_in
  end

  get '/gpg/?' do
    redirect '/pgp'
  end

  get '/pgp/?' do
    title "Sign In with a PGP Key"
    erb :pgp
  end

end
